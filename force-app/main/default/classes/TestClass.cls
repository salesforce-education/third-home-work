public class TestClass {
    // Created By Andrii
    public static Integer sum(Integer i1, Integer i2) {
        return i1 + i2;
    }

    public static Integer multiply(Integer i1, Integer i2) {
        return i1 * i2;
    }

    public static Integer divide(Integer i1, Integer i2) {
        return i1 / i2;
    }
}